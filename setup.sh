#!/bin/bash

# Update the packages
apt update
apt upgrade --yes

# Setup tzdata, which comes as a dependency of some other package that I need
# set noninteractive installation
export DEBIAN_FRONTEND=noninteractive
#install tzdata package
apt-get install -y tzdata
# set your timezone
ln -fs /usr/share/zoneinfo/America/Mexico_City /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata

# Install dependencies needed to build pizarra
apt install build-essential libgtk-3-dev curl --yes

# second setup rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
